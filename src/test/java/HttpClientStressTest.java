import com.ning.http.client.AsyncCompletionHandler;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Response;
import com.ning.http.client.providers.netty.NettyResponse;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;

public class HttpClientStressTest {

    private static final Logger logger = LoggerFactory.getLogger(HttpClientStressTest.class);

    private static final int PORT = 18080;
    Server server;

    ExecutorService svc;

    @Before
    public void setUp() throws Exception {
        server = new Server(PORT);

        server.setHandler(new SimpleHandler());

        server.start();

        svc = Executors.newCachedThreadPool();
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
        svc.shutdownNow();
    }

    @Test
    public void testLotsOfRequests() throws Exception {
        final int requests = 10000;

        Thread.sleep(100);

        final AsyncHttpClient asyncHttpClient = new AsyncHttpClient();

        final AtomicInteger impossibleNullStatusFound = new AtomicInteger();

        final Semaphore reqSemaphore = new Semaphore(0);

        final AsyncCompletionHandler<Response> handler = new AsyncCompletionHandler<Response>() {
            @Override
            public Response onCompleted(Response response) throws Exception {
                reqSemaphore.release();

                if (response instanceof NettyResponse && !response.hasResponseStatus()) {
                    impossibleNullStatusFound.incrementAndGet();
                }

                return response;
            }

            @Override
            public void onThrowable(Throwable t) {
                reqSemaphore.release();
                logger.warn("got throwable", t);
            }
        };

        for (int i = 0; i < requests; i++) {
            try {
                asyncHttpClient.prepareGet("http://127.0.0.1:" + PORT + "/").execute(handler);
            } catch (IOException e) {
                logger.warn("get failed", e);
            }
        }
        reqSemaphore.acquire(requests);

        assertEquals("Total of " + requests + " requests", 0, impossibleNullStatusFound.get());
    }

    static class SimpleServlet extends HttpServlet {
        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            resp.getWriter().write("ok");
        }
    }

    static class SimpleHandler extends AbstractHandler {
        @Override
        public void handle(String target, Request baseRequest, HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
            response.setContentType("text/plain;charset=utf-8");
            response.setStatus(HttpServletResponse.SC_OK);
            baseRequest.setHandled(true);
            response.getWriter().println("ok");
        }
    }
}
